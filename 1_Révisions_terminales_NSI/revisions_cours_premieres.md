---
title: "Chapitre révisions terminales"
subtitle: "Rappels cours première"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---  

# Révisions du programme de première.  

## Les tableaux en python. (listes en python) 

* Déclaration d'un tableau en python:  
```python
    tableau = [] # Déclaration d'un tableau vide
    ou 
    tableau = [ 8, 10, 12] # Déclaration d'un tableau complété   
```  

* Propriétés communes aux tableaux:  

```python
    tableau = [8, 10, 12]
    len(tableau) # len donne la longueur du tableau.
    3  

    tableau = [ 8, 10, 12]
    tableau[1] # On met entre crochet l'indice de l'élément dont on souhaite avoir la valeur.
    10 # L'indice des tableaux commence à 0.  

    tableau = [8, 10, 12]
    tableau[0] = 6  # On affecte une nouvelle valeur à l'élément d'indice 0.
    print(tableau)
    [6, 10, 12]
```  

* Propriétés propres aux tableaux en python:  

__Ajouter des éléments à la fin du tableau__  

```python
    tableau = [8, 10, 12]
    tableau.append(14)  # La méthode append ajoute n'importe quel élément à la fin du tableau.
    print(tableau)
    [8, 10, 12, 14]  

    tableau = [8, 10, 12]
    tableau.extend([14, 16]) #La méthode extend fusionne un tableau à la fin du tableau.
    print(tableau)
    [8, 10, 12, 14, 16]  

    tableau = [0, 1, 2]
    tableau = tableau + [4, 5] # On peut fusionner des tableaux par concaténation.
    print (tableau)
    [0, 1, 2, 4, 5]
```  

__Retirer des éléments du tableau__  

``` python  
    tableau = [0, 1, 2]
    tableau.pop() # La méthode pop retire le dernier élément du tableau.
    print(tableau)
    [0, 1]  

    tableau = [0, 2, 4, 6, 8, 10]
    del(tableau[1])  # La fonction del supprime un élément dont on connait l'indice.
    print(tableau)
    [0, 4, 6, 8, 10]  

    tableau = [0, 2, 4, 6, 8, 10]
    del(tableau[1:3]) # La fonction del peut supprimer plusieurs éléments.
    print(tableau)
    [0, 6, 8, 10]  

    tableau = [ 'a', 'b', 'c', 'd']
    tableau.remove('b') # La méthode remove() supprime un élément dont on connait le contenu.
    print(tableau)
    [ 'a', 'c', 'd']
```  

* Quelques autres méthodes:  

| méthode | utilité |
|:--------|:--:|
| sort() | Trie une liste selon les valeurs du codage des éléments |
| index(élément) | Renvoie l'indice de l'élément passé en paramètre |  
| reverse() | Inverse les éléments de la liste |  
| split(élément séparateur) | Méthode appliquée à une chaine de caractères. Elle coupe la chaine dès qu'elle rencontre l'élément séparateur. Elle stocke les éléments dans une liste. |
| insert(indice,valeur) | Méthode qui insère à l'indice fixé en paramètre 1 la valeur fixée en paramètre 2 |  

* Revoir le cours sur la mutabilité des listes (tableaux) en python.  

* Construire simplement des listes en python:  

__Créer une liste comportant les mêmes éléments__  

```python
   >>>[0]*10  # On crée une liste comportant l'élément souhaité et on la multiplie.
   >>>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]  
```  

__Utilisation de la fonction `list`__  
```python
>>>list(range(10)) #La fonction list permet de créer une liste en paramétrant range.
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

>>> list(range(2, 10, 2))
[2, 4, 6, 8]
```  

__Utilisation des boucles__  

```python
    liste = []
    for i in range(20):
        if not i%2:
            liste.append(i)
    print(liste)

    [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
```  

* Création de listes par compréhension:  

```python
>>>[0 for _ in range(10)] # On crée une liste comportant que des 0
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

>>>[i for i in range(10)] # On crée une liste comportant les chiffres de 0 à 9
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]  

>>>[i for i in range(20) if not i%2] 
# création d'une liste en intégrant une structure conditionnelle
[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]  

>>>[[0 for i in range(5)] for j in range(3)]
[[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
# Création d'une liste à deux dimensions. On pourrait aussi utiliser une double boucle pour le faire.
```

## Les tuples.  

* Création d'un tuple:  

__Avec plusieurs éléments__  

```python
>>> (1, 2, 3, 4, 5) # Les éléments sont encadrés par des parenthèses et séparés par des virgules
(1, 2, 3, 4, 5)
```  

__Avec un élément__  

```python
>>>(1,)  ou  1,
(1,)
```  

__Création d'un tuple sur une chaine de caractères__  

```python
>>> tuple("Marianne") # On applique la fonction `tuple` à la chaine de caractères.
('M', 'a', 'r', 'i', 'a', 'n', 'n', 'e')  
```  

__Création d'un tuple par compréhension__  

```python
>>> tuple(i*i for i in range(10))
(0, 1, 4, 9, 16, 25, 36, 49, 64, 81)  
```  

* Accéder à un élément d'un tuple:  

```python
>>> tuple1 = (1, 2, 3, 4)
>>> tuple1[1] # Comme pour les listes, on met entre crochets l'indice de l'élément.
2
```  

* Modifier un élément d'un tuple:  

Il est impossible de modifier directement un élément d'un tuple.  

* Ajouter des éléments à un tuple:  

```python
>>> tuple1 = (1, 2)
>>> tuple2 = (3, 4)
>>> tuple3 = tuple1 + tuple2 # On fusionne deux tuples dans un troisième par concaténation.
>>> tuple3
(1, 2, 3, 4)
```  

* La fonction len peut s'appliquer aux tuples ainsi que les méthodes .index() ou .count()  

## Les dictionnaires.  

* Construire un dictionnaire:  

```python
>>> dico_vide = {} # Création d'un dictionnaire vide.
>>> dico = {"Dupond": "Jean", "Renard": "Jeanne"} #Création d'un dictionnaire rempli.
```

Un dictionnaire est donc composé de paires `cle`: `valeur` 

__Construire un dictionnaire en compréhension__  

```python
>>> {n : n**2 for n in range(1, 11)}
{1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81, 10: 100}
```  

* Modifier un dictionnaire:  

__Ajouter un élément__  

```python
>>> dico = {"Dupond": "Jean", "Renard": "Jeanne"}
>>> dico["Lambert"] = "Louis" # On ajoute directement une paire cle: valeur
>>> dico
{'Dupond': 'Jean', 'Renard': 'Jeanne', 'Lambert': 'Louis'}
```

__Supprimer un élément__  

```python
>>> dico = {'Dupond': 'Jean', 'Renard': 'Jeanne', 'Lambert': 'Louis'}
>>> del(dico["Renard"])
>>> dico
{'Dupond': 'Jean', 'Lambert': 'Louis'}
```

* Parcourir un dictionnaire:  

__Parcourir par les clés__  

On utilise la méthode `.keys()`

```python
>>> dico = {'Dupond': 'Jean', 'Renard': 'Jeanne', 'Lambert': 'Louis'}
>>> dico.keys()
dict_keys(['Dupond', 'Renard', 'Lambert'])
>>> for nom in dico.keys():
    print(dico[nom])

Jean
Jeanne
Louis
```
__Parcourir par les valeurs__  

On utilise cette fois-ci la méthode `.values()`  

__Parcourir les paires cle: valeur__

On utilise cette fois-ci la méthode `.items()`