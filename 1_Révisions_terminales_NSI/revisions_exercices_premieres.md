---
title: "Chapitre révisions terminales"
subtitle: "Exercices cours première"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---  

# Exercices rappels première.  

## Exercices Tableaux:  

### Exercice 1 : Le bulletin &#x1F3C6;  

 1) Vous allez commencer par créer une fonction __creation_liste()__ qui crée une liste contenant les valeur 10, 14 et 8. La fonction retournera une liste qui sera stockée dans la variable __notes__.

 2) Vous allez créer une fonction __affichage(texte, param)__ qui affiche la liste __notes__ ou autres.

Par exemple :  
 affichage("Notes", notes)

 Notes : [10, 14, 8]
   
 3) Vous allez ensuite créer une fonction __entrer_notes(param)__ qui demande à l'utilisateur de rentrer des notes. Les notes seront stockées dans la liste notes. Pour cela, on demandera à chaque fois si l'utilisateur veut entrer une note. Si la réponse est __oui__ on demande la note, on la stocke et on repose la question, si la réponse est __non__ on retourne la liste complétée. Si la réponse est différente de __oui ou non__ on repose la question après avoir précisé que l'entrée était mauvaise.  

 4) Vous allez créer une fonction __calcul_moyenne(param)__ qui retourne la moyenne des éléments de la liste __notes__.

 Fonction principale : 

 - Vous allez créer une fonction appelée __principale()__  
 - Vous allez commencer par créer la liste __notes__ remplie des trois valeurs.  
 - Vous allez ensuite afficher la liste __notes__
 - vous demanderez ensuite s'il faut ajouter des notes. Puis un fois terminé on affiche de nouveau la liste __notes__
 - Enfin, vous calculez la moyenne des valeurs de la liste __notes__ et vous l'affichez.

Questions complémentaires :  

- On pourra créer une fonction qui recherche la meilleure note. (On n'utilisera pas une méthode existante.)  

- On pourra chercher la note minimale. (On n'utilisera pas une méthode existante.) 

- On pourra proposer de supprimer la dernière note entrée.  

- On pourra proposer de supprimer une note dont on connait la valeur.  

### Exercice 2 : Cryptographie  le code de César. &#x1F3C6;&#x1F3C6;    

C'est une méthode ancienne de cryptographie qui consiste à réaliser un décalage constant dans l'ordre alphabétique. Ce mode de cryptographie a été rapidement abandonné car une fois qu'on connait la méthode, le décryptage est très simple. Cependant, vous allez dans cet exercice reproduire le code de César.  

 1) Dans un premier temps, vous allez créer une fonction qui retourne une liste comportant l'ensemble de l'alphabet dans l'ordre croissant. Cette liste sera stockée dans une variable nommée __alphabet__
``` python  
    >>>[ 'a', 'b', ...]
```

 2) Dans un deuxième temps, vous allez créer une fonction __decalage(n, param_alphabet)__ qui prendra en paramètres un entier n qui représente le décalage à effectuer et la variable alphabet. La fonction retournera une liste nommée __liste_decalee__ qui aura subi un décalage de n. (On pourra utiliser les méthodes .pop() et insert())  
``` python
    liste_decalee = decalage(2)
    print(liste_decallee)
    ['y', 'z', 'a', 'b', 'c', ...]
```
Remarque: Attention, il faut prendre en compte la mutabilité des listes.


 3) Vous allez créer une fonction qui va demander à l'utilisateur le mot à crypter. Cette fonction retournera une chaine de caractères qui sera sotckée dans la variable __mot__. (On se contentera d'un mot pour le moment.)

 4) Vous allez créer une fonction qui va demander à l'utilisateur la clé de cryptage c'est à dire ici le décalage souhaité. La fonction retournera un entier stocké dans une variable nommée __valeur__

 5) Créer une fonction qui prendra trois paramètres, la lettre à crypter, la variable alphabet et la variable liste_decalee. Elle retournera la valeur crypter du caractère passé en paramètre. (On pourra utilisé la méthode .index())

 6) Vous allez créer une fonction qui affiche un caractère passé en paramètre.

Fonction principale :  

 - Créer une liste représentant l'alphabet dans l'ordre croissant stockée dans la variable __alphabet__
 - Demander à l'utilisateur la clé de cryptage.
 - Créer une liste décalée en utilisant la clé de cryptage et la variable __alphabet__.  
 - Demander le mot à l'utilisateur.  
 - Crypter une lettre du mot et l'afficher. (On répètera cette opération pour toutes les lettres du mot.)  

### Exercice 3: Premières applications de listes par compréhension. &#x1F3C6;    

1) Créer une liste par compréhension qui contient  les 20 premiers entiers en commençant par 0.  

2) Créer une liste par compréhension qui contient les éléments [3, 6, 9, ...,24].  

3) Créer une listepar compréhension en deux dimensions 4x4 qui ne contient que des 0.
   
### Exercice 4: &#x1F3C6;&#x1F3C6;
* Créer une liste par compréhension qui a pour affichage:  
(On créera une fonction pour créer la liste et on utilisera le cours pour créer une fonction qui affichera cette liste.)  

1 0 0 0 0 0 0 0 0 0  
0 1 0 0 0 0 0 0 0 0  
0 0 1 0 0 0 0 0 0 0  
0 0 0 1 0 0 0 0 0 0  
0 0 0 0 1 0 0 0 0 0  
0 0 0 0 0 1 0 0 0 0  
0 0 0 0 0 0 1 0 0 0  
0 0 0 0 0 0 0 1 0 0  
0 0 0 0 0 0 0 0 1 0  
0 0 0 0 0 0 0 0 0 1   

* Créer une liste par compréhension qui a pour affichage:

1 0 0 0 0 0 0 0 0 0  
1 1 0 0 0 0 0 0 0 0  
1 1 1 0 0 0 0 0 0 0  
1 1 1 1 0 0 0 0 0 0  
1 1 1 1 1 0 0 0 0 0  
1 1 1 1 1 1 0 0 0 0  
1 1 1 1 1 1 1 0 0 0  
1 1 1 1 1 1 1 1 0 0  
1 1 1 1 1 1 1 1 1 0  
1 1 1 1 1 1 1 1 1 1  

* Créer une liste par compréhension qui a pour affichage:  

1 0 0 0 0 0 0 0 0 1  
0 1 0 0 0 0 0 0 1 0  
0 0 1 0 0 0 0 1 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 1 0 0 0 0 1 0 0  
0 1 0 0 0 0 0 0 1 0  
1 0 0 0 0 0 0 0 0 1  

## Exercices tuples &#x1F3C6;  

__1)__ Définir une fonction `somme` qui renvoie la somme des éléments d'un tuple.

_Exemple :_

```python
>>> somme((1, 5, 8, 10))
24
```

__2)__ Définir une fonction `inverse` qui inverse les éléments d'un tuple pour construire un nouveau tuple et le renvoyer.

_Exemple :_

```python
>>> inverse(('a', 2, 3))
(3, 2, 'a')
```

__AIDE__ : On pourra notamment utiliser les fonctions `list` ou `tuple` permettant repectivement de convertir une séquence quelconque en liste ou en tuple

__3)__ On souhaite définir un prédicat `is_right_angle` admettant un triplet `t` pour argument, ce triplet contenant la longueur des trois côtés d'un triangle (exprimés dans la même unité).  
Le prédicat devra renvoyé `True` si le triangle est rectangle, `False` dans le cas contraire

_Exemple :_

```python
>>> is_right_angle((6, 8, 10))
True
```

__4)__ 🥇 Définir une fonction `infos_notes` qui accepte une liste de notes en paramètre et qui renvoie un triplet contenant dans l'ordre la moyenne arrondi au dixième, la note la plus basse ainsi que la note la plus haute.

_Exemple :_

```python
>>> infos_notes([15, 12, 10, 8, 17, 11])
(12.2, 17, 8)
```  

## Exercices Dictionnaires:  

### Exercice 1 : Pokemons &#x1F3C6;
(_Extrait de Prépabac NSI, Hatier_)

On modélise des informations (nom, taille et poids) sur des Pokémons de la façon suivante :

```python
exemple_pokemons = {'Bulbizarre' : (70, 7),
                    'Herbizarre' : (100, 13),
                    'Abo' : (200, 7),
                    'Jungko' : (170, 52)}
```

Par exemple, Bulbizarre est un Pokémon qui mesure 70cm et qui pèse 7kg.

__1)__ Quel est de type de `exemple_pokemons`?    
__2)__ Quelle instruction permet d'ajouter à cette structure de données le Pokémon Goupix qui mesure 60cm et qui pèse 10kg ?   
__3)__ On donne le code suivant:

```python
def le_plus_grand(pokemons):
    grand = None
    taille_max = None
    for (nom, (taille, poids)) in pokemons.items():
        if taille_max is None or taille > taille_max:
            taille_max = taille
            grand = nom
    return (grand, taille_max)
```

__a-__ Que vaut l'expression `le_plus_grand(exemple_pokemons)` ?  
__b-__ Ecrire le code d'une fonction `le_plus_leger` qui prend un dictionnaire de Pokémons en paramètre et qui renvoie un tuple dont la première composante est le nom du Pokémon le plus léger et la deuxième correspondante est son poids.

```python
>>> le_plus_leger(exemple_pokemons) 
('Bulbizarre', 7)
```
__c-__ 🥇 Réécrire le code de la fonction `le_plus_grand` en parcourant cette fois-ci les valeurs du dictionnaire.

__4)__ Ecrire le code d'une fonction `taille` qui prend un dictionnaire de Pokémons en paramètre ainsi que le nom d'un Pokémon et qui renvoie la taille de ce Pokémon

```python
>>> taille(exemple_pokemons, 'Abo') 
200
```

### Exercice 2 : Dictionnaires par compréhension &#x1F3C6;&#x1F3C6;

Construire les dictionnaires par compréhensions suivants :  

__1)__ __clés__ : les nombres entiers de 0 à 100 inclus ; __valeurs__ :  leur racine carrée.  

__2)__ __clés__ : les nombres pairs de 0 à 20 inclus ; __valeurs__ : leur cube  

__3)__ __clés__ :  les caractères ASCII imprimables ; __valeurs__ : le code décimal associé  

__4)__ Soit la liste `pays = ['France', 'Espagne', 'Italie', 'Allemagne', 'Belgique']`  
__clés :__ les pays présents dans `liste` ; __valeurs :__ le nombre de caractères utilisés pour écrire ce pays.  


### Exercice 3 : Magasin en ligne &#x1F3C6;&#x1F3C6;

(_Extrait de https://bioinfo.mnhn.fr/abiens/ISUP5G3/docu/ISUP-elemprog-exos_2016-09-16-student.pdf_)

Dans cet exercice, nous nous familiarisons avec les manipulations de dictionnaires sur une thématique de magasin en ligne.
_«Chez Geek and sons tout ce qui est inutile peut s’acheter, et tout ce qui peut s’acheter est un peu trop cher.»_  
La base de prix des produits de _Geek and sons_ est représentée en Python par un dictionnaire de type `dict[str:float]` avec :

* les noms de produits, de type `str`, comme clés
* les prix des produits, de type `float`, comme valeurs associées.


__1)__ Donner une expression Python pour construire un dictionnaire `base_geek_sons` comprenant les produits correspondant à la table suivante associés à leur prix repectif.

|Nom du produit| Prix TTC|
|:----:|:----:|
|Sabre laser| 229.0|
|Mitendo DX |127.30|
|Coussin Linux |74.50|
|Slip Goldorak |29.90|
|Station Nextpresso| 184.60|


__2)__ Définir le prédicat `disponibilite` qui étant donné un nom de produit `prod` (_sous forme de chaîne de caractères_) et une base de prix `prix` (_sous forme de dictionnaire_), renvoie `True` si le produit est présent dans la base, `False` sinon.

__3)__ Définir la fonction `prix_moyen` qui, étant donné une base de prix `base_prix` (contenant au moins un produit), renvoie le prix moyen des produits disponibles.

Par exemple :

```python
>>> prix_moyen({'Sabre Laser': 229.0, 
                'Mitendo DX': 127.30,
                'Coussin Linux' : 74.50,34, 
                'Slip Goldorak' : 29.90,
                'Station Nextpresso' : 184.60})
129.06
```

__4)__ Définir la fonction `fourchette_prix` qui, étant donné un prix minimum `mini`, un prix maximum `maxi` et une base de prix `base_prix`, renvoie  l’ensemble des noms de produits disponibles dans cette fourchette de prix sous forme de tuple de chaînes de caratères.

Par exemple :

```python
>>> fourchette_prix(50.0, 200.0, 
                    {'Sabre Laser': 229.0,
                    'Mitendo DX': 127.30, 
                    'Coussin Linux' : 74.50,
                    'Slip Goldorak' : 29.90, 
                    'Station Nextpresso' : 184.60})

('Coussin Linux', 'Mitendo DX', 'Station Nextpresso')
```

__5)__ Le panier est un concept omniprésent dans les sites marchands, Geeks and sons n’échappepas à la règle. En Python, le panier du client sera représenté par un dictionnaire de type `dict[str:int]` avec :

* les noms de produits comme clés
* une quantité d’achat comme valeurs associées.

Donner une expression Python correspondant à l’achat de 3 sabres lasers, de 2 coussins Linux, et de 1 slip Goldorak.

__6)__  Définir le prédicat `tous_disponibles` qui, étant donné un panier d’achat `panier` et une base de prix `base_prix` renvoie `True` si tous les produits demandés sont disponibles, `False` sinon.

__7)__ Définir la fonction `prix_achats` qui, étant donné un panier d’achat `panier` et une base de prix `base_prix`, renvoie le prix total correspondant.

Par exemple :

```python
>>> prix_achats({'Sabre Laser': 3, 'Coussin Linux': 2, 'Slip Goldorak': 1},
                {'Sabre Laser': 229.0,
                'Mitendo DX' : 127.30, 
                'Coussin Linux' : 74.50,
                'Slip Goldorak' : 29.90,
                'Station Nextpresso' : 184.60})
865.9
```
_Remarque_ : on supposera que tous les articles du paniers sont disponibles dans la base de produits.